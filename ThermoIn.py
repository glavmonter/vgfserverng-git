# -*- coding: utf-8 -*-
__author__ = 'Vladimir Meshkov <glavmonter@gmail.com>'

import time
import can
from VGFManager import VGFManager
from VGFManager import ThermoClass


can.rc['interface'] = 'socketcan_ctypes'
from can.interfaces.interface import Bus
can_interface = 'can0'
can.set_logging_level('info')


def main():
    bus = Bus(can_interface)
    listeners = [ThermoClass(DeviceId=1, bus=bus), ThermoClass(DeviceId=2, bus=bus),
                 ThermoClass(DeviceId=3, bus=bus)]
    manager = VGFManager(bus, listeners, 0.25)

    try:
        while True:
            time.sleep(10)

    except KeyboardInterrupt:
        manager.running.clear()
        bus.shutdown()


if __name__ == '__main__':
    main()
