#!/usr/bin/env python3
# -*- coding: utf-8 -*-
__author__ = 'Vladimir Meshkov <glavmonter@gmail.com>'

import signal
import socket
import select
import threading
import sys
import can
from datetime import datetime

from os.path import expanduser
from configparser import ConfigParser
from argparse import ArgumentParser

from daemonize import Daemonize
from VGF.protocol_tcp_pb2 import ClientServer
from VGF.VGFManager import VGFManager
from VGF.ClientThread import ClientThread

can.rc['interface'] = 'socketcan_ctypes'
from can.interfaces.interface import Bus

home_path = expanduser('~')
parser = ConfigParser()
parser.read(home_path + '/.vgfserverrc')

can_interface = parser.get(section='server', option='can_interface', fallback='can0')
tcp_port = int(parser.get(section='server', option='port', fallback='9999'))
tcp_address = parser.get(section='server', option='address', fallback='127.0.0.1')

database_address = parser.get(section='database', option='address', fallback='127.0.0.1')
database_port = int(parser.get(section='database', option='port', fallback='27017'))
database_name = parser.get(section='database', option='database_name', fallback='default')


parser = ArgumentParser()
parser.add_argument("-d", "--daemonize", action="store_true",
                    help="Стать демоном")
args = parser.parse_args()

# can.set_logging_level('info')


class SigError(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        if self.value == signal.SIGHUP:
            return repr('SIGHUP')
        elif self.value == signal.SIGUSR1:
            return repr('SIGUSR1')
        elif self.value == signal.SIGUSR2:
            return repr('SIGUSR2')


def sighandler(signum, frame):
    raise SigError(signum)


class ManagerThread(threading.Thread):
    def __init__(self, ip, port, socket):
        threading.Thread.__init__(self)
        self._ip = ip
        self._port = port
        self._socket = socket

        self.running = threading.Event()
        self.running.set()

    def run(self):
        import logging
        logging.basicConfig(level=logging.DEBUG)
        self.logger = logging.getLogger('Manager')
        self._socket.setblocking()

        try:
            self.logger.debug('Connected {} at {}'.format(self._ip, self._port))
            while self.running.is_set():
                ready = select.select([self._socket], [], [], 0.5)
                if ready[0]:
                    data = self._socket.recv(2048)
                    if len(data) == 0:
                        self.logger.debug('Remote host closed connection')
                        self.running.clear()
                        self._socket.close()
                        break
                    self.process_data_in(data)

        except:
            self.logger.exception('Problem handling request')

    def process_data_in(self, data):
        pass


class Server(object):
    def __init__(self, hostname, port, clients, vgfmanager):
        import logging
        logging.basicConfig(level=logging.DEBUG)
        self.logger = logging.getLogger('server')
        self.logger.setLevel(logging.DEBUG)

        file_handler = logging.FileHandler(filename='/tmp/test.log', mode='a')
        file_handler.setLevel(logging.INFO)
        formatter = logging.Formatter(fmt='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                                      datefmt='%Y-%m-%d, %H:%M:%S')
        file_handler.setFormatter(formatter)
        self.logger.addHandler(file_handler)

        self.hostname = hostname
        self.port = port
        self.clients = clients
        self.vgfmanager = vgfmanager
        self._sync_task = None
        self._syncing = False
        self.logger.info('Started server at {}'.format(datetime.strftime(datetime.now(), '%Y-%m-%d, %H:%M:%S')))
        self.logger.info('Hostname: {}'.format(self.hostname))
        self.logger.info('Database name: {}'.format(database_name))

    def start(self):
        self.logger.debug('listening')
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.socket.bind((self.hostname, self.port))
        self.socket.listen(1)

        while True:
            try:
                (clientsock, (ip, port)) = self.socket.accept()

                for c in self.clients:
                    if not c.running.is_set():
                        self.logger.debug('Client {} stopped. Delete from list and join.'.format(c))
                        c.join()
                        self.clients.remove(c)

                data = clientsock.recv(2048)
                if len(data) > 0:
                    protocol_in = ClientServer()
                    protocol_in.ParseFromString(data)

                    if protocol_in.type == ClientServer.CLIENT_LOCAL:
                        self.logger.debug('Got connection Client Local')
                        process = ClientThread(ip=ip, port=port, socket=clientsock, type=ClientServer.CLIENT_LOCAL,
                                               vgfmanager=self.vgfmanager)
                        process.daemon = False
                        self.clients.append(process)
                        process.start()
                        self.logger.debug('Started process {}'.format(process))

                    elif protocol_in.type == ClientServer.MANAGER:
                        self.logger.debug('Got connection Manager. Reject')
                        clientsock.close()
                    else:
                        self.logger.debug('Got connection Unknown. Reject')
                        clientsock.close()

            except SigError as msg:
                if msg.value == signal.SIGUSR1:
                    if not self._syncing:
                        self._syncing = True
                        self.logger.info('Starting sync')
                        if self._sync_task is None:
                            msg = can.Message(arbitration_id=0x80, extended_id=False, data=[])
                            self._sync_task = can.send_periodic(can_interface, msg, 1.0)
                        else:
                            self._sync_task.start()
                    else:
                        self.logger.info('Stopping sync')
                        self._syncing = False
                        if self._sync_task is not None:
                            self._sync_task.stop()

                elif msg.value == signal.SIGUSR2:
                    self.vgfmanager.process_sigusr2()
                elif msg.value == signal.SIGHUP:
                    self.vgfmanager.process_sighup()
            except:
                return


def main():
    signal.signal(signal.SIGUSR1, sighandler)
    signal.signal(signal.SIGUSR2, sighandler)
    signal.signal(signal.SIGHUP, sighandler)

    clients = []

    import logging

    if args.daemonize:
        logging.basicConfig(format='%(filename)s:%(lineno)d# %(levelname)-8s [%(asctime)s] %(message)s',
                            datefmt='%Y-%m-%d, %H:%M:%S',
                            level=logging.DEBUG,
                            filename='/tmp/VGFServer.log',
                            filemode='w')
    else:
        logging.basicConfig(format='%(filename)s:%(lineno)d# %(levelname)-8s [%(asctime)s] %(message)s',
                            datefmt='%Y-%m-%d, %H:%M:%S',
                            level=logging.DEBUG)

    bus = Bus(can_interface)
    manager = VGFManager(bus=bus, database_address=database_address, database_port=database_port,
                         database_name=database_name, timeout=0.25)
    can.set_logging_level('info')
 
    # logging.basicConfig(level=logging.DEBUG)
    logger = logging.getLogger('main')
    logger.setLevel(logging.DEBUG)

    logger.debug('Hello')

    server = Server(tcp_address, tcp_port, clients, manager)
    try:
        logger.info('Listening')
        server.start()
    except KeyboardInterrupt:
        logger.exception('Keyboard interrupt')
    except SigError:
        logger.exception('OSError')
    except:
        logger.exception('Unexpected exception,')

    finally:
        logger.info('Shutting down')
        for process in clients:
            logger.info('Shutting down process {}'.format(process))
            process.running.clear()
            process.join()

        manager.running.clear()
        bus.shutdown()

    logger.info('All done')
    sys.exit(0)

if args.daemonize:
    daemon = Daemonize(app='test_app', pid='/tmp/VGFServer.pid', action=main)
    daemon.start()

if __name__ == '__main__':
    main()
