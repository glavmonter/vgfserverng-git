#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import time
import logging
import can

from can.notifier import Notifier
from can.CAN import Listener
from can.interfaces.interface import Bus
from can.interfaces.interface import Message

import VGFThermo_pb2
import VGFPlatinum_pb2

logging.basicConfig(level=logging.INFO)


def GENID(ID, MSG):
    return (MSG << 4) | ID


def GETMSG(cobid):
    return (cobid >> 4) & 0x0F


CANMSG_SYNC = 0x08
CANMSG_DTH = 0x0B
CANMSG_DATA = 0x0E


class MyListenerThermo(Listener):
    def __init__(self, DeviceId, bus):
        self._DeviceId = DeviceId
        self._bus = bus
    
    def on_message_received(self, msg):
        if msg.arbitration_id == 0x80:
            self.SendPb()
    
    def SendPb(self):
        payload = VGFThermo_pb2.Payload()
        payload.DeviceId = self._DeviceId

        for z in range(8):
            DtH = payload.DtH.add()
            DtH.ZoneNumber = z
            DtH.CurrentT = 12.5*(z+2)
            DtH.PresetT = 36.6*(z+1)
            DtH.PwmOut = 9653
            DtH.PidError = 0.3
            DtH.Sendiag = VGFThermo_pb2.DeviceToHost.SENDIAG_OFLO_CLAMP
            DtH.Mode = VGFThermo_pb2.MODE_STOPPED

        strout = payload.SerializeToString()

        bytes_to_send = len(strout)
        # print('Num of bytes: ', bytes_to_send)
              
        num_messages = bytes_to_send // 7
        msg = Message()
        
        msg.arbitration_id = GENID(self._DeviceId, CANMSG_DTH)
        msg.dlc = 6
        msg.data = [bytes_to_send & 0xFF, (bytes_to_send & 0xFF00) >> 8, 0, 0, 0, 0]
        msg.id_type = False
        self._bus.send(msg)

        for m in range(num_messages):
            mmm = Message()
            mmm.arbitration_id = GENID(self._DeviceId, CANMSG_DATA)
            mmm.dlc = 8
            mmm.id_type = False
            
            mmm.data = bytearray(1)
            mmm.data[0] = m
            mmm.data = mmm.data + bytearray(strout[m * 7: (m + 1)*7])
            # print('Send: ', list(mmm.data))
            self._bus.send(mmm)
        
        if bytes_to_send % 7:
            mmm = Message()
            mmm.arbitration_id = GENID(self._DeviceId, CANMSG_DATA)
            mmm.dlc = 1 + bytes_to_send % 7
            mmm.id_type = False
            
            mmm.data = bytearray(1)
            mmm.data[0] = bytes_to_send // 7
            mmm.data = mmm.data + bytearray(strout[bytes_to_send - bytes_to_send % 7: bytes_to_send])
            # print('Send: ', list(mmm.data))
            self._bus.send(mmm)


class MyListenerPt(Listener):
    def __init__(self, DeviceId, bus):
        self._DeviceId = DeviceId
        self._bus = bus

    def on_message_received(self, msg):
        if msg.arbitration_id == 0x80:
            self.SendPb()

    def SendPb(self):
        payload = VGFPlatinum_pb2.Payload()
        payload.DeviceId = self._DeviceId

        payload.DtH.TemperaturePt.extend([1.0, 1.1, 1.3, 0.0])
        payload.DtH.SendiagPt.extend([0,
                                      VGFPlatinum_pb2.DeviceToHost.SENDIAG_OFLO_CLAMP,
                                      VGFPlatinum_pb2.DeviceToHost.SENDIAG_OFLO_NEG,
                                      VGFPlatinum_pb2.DeviceToHost.SENDIAG_SHORT])
        payload.DtH.TemperatureDs.extend([13.0, 15.0, 16.0, 17.0, 18.0, 17.0, 18.0, 19.0, 12.9])

        strout = payload.SerializeToString()

        bytes_to_send = len(strout)
        # print('Num of bytes: ', bytes_to_send)

        num_messages = bytes_to_send // 7
        msg = Message()

        msg.arbitration_id = GENID(self._DeviceId, CANMSG_DTH)
        msg.dlc = 6
        msg.data = [bytes_to_send & 0xFF, (bytes_to_send & 0xFF00) >> 8, 0, 0, 0, 0]
        msg.id_type = False
        self._bus.send(msg)

        for m in range(num_messages):
            mmm = Message()
            mmm.arbitration_id = GENID(self._DeviceId, CANMSG_DATA)
            mmm.dlc = 8
            mmm.id_type = False

            mmm.data = bytearray(1)
            mmm.data[0] = m
            mmm.data = mmm.data + bytearray(strout[m * 7: (m + 1)*7])
            # print('Send: ', list(mmm.data))
            self._bus.send(mmm)

        if bytes_to_send % 7:
            mmm = Message()
            mmm.arbitration_id = GENID(self._DeviceId, CANMSG_DATA)
            mmm.dlc = 1 + bytes_to_send % 7
            mmm.id_type = False

            mmm.data = bytearray(1)
            mmm.data[0] = bytes_to_send // 7
            mmm.data = mmm.data + bytearray(strout[bytes_to_send - bytes_to_send % 7: bytes_to_send])
            # print('Send: ', list(mmm.data))
            self._bus.send(mmm)


def main():
    can.rc['interface'] = 'socketcan_ctypes'
    can_interface = 'vcan0'
    bus = Bus(can_interface)

    listener = [MyListenerThermo(DeviceId=1, bus=bus), MyListenerThermo(DeviceId=2, bus=bus),
                MyListenerThermo(DeviceId=3, bus=bus), MyListenerPt(DeviceId=4, bus=bus)]
    notifier = Notifier(bus, listener, 100)
    
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        notifier.running.clear()
        bus.shutdown()

if __name__ == '__main__':
    main()

