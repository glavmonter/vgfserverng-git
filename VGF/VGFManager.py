# -*- coding: utf-8 -*-
import threading
import datetime
import queue

from os.path import expanduser

import VGF.VGFThermo_pb2 as VGFThermo_pb2
from VGF.VGFThermo import ThermoClass
from VGF.VGFPlatinum import PlatinumClass
from VGF.VGFDisplay import DisplayClass
from pymongo import MongoClient, ASCENDING

import VGF.Autos as Autos

__author__ = 'Vladimir Meshkov <glavmonter@gmail.com>'

# TODO Откоментировать хорошенько
# TODO Проверить все на статические поля


class VGFManager(object):
    # TODO Добавить хранение всех состояний
    def __init__(self, bus, database_address, database_port, database_name, timeout=None):
        self.display = DisplayClass(DeviceId=5, bus=bus)
        self.controller_pt = PlatinumClass(DeviceId=4, bus=bus)
        self.vgfthermos = {ThermoClass(DeviceId=1, bus=bus), ThermoClass(DeviceId=2, bus=bus),
                           ThermoClass(DeviceId=3, bus=bus)}

        import logging
        self._logger = logging.getLogger('Manager')
        self._logger.setLevel(logging.INFO)

        file_handler = logging.FileHandler(filename='/tmp/test.log', mode='a')
        file_handler.setLevel(logging.INFO)
        formatter = logging.Formatter(fmt='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                                      datefmt='%Y-%m-%d, %H:%M:%S')
        file_handler.setFormatter(formatter)
        self._logger.addHandler(file_handler)

        self._program_path = expanduser('~') + '/program.json'

        self._mode_auto = False
        self._mode_parser = Autos.ModeParser()

        self.bus = bus
        self.timeout = timeout

        self._sync_number = 0

        self.running = threading.Event()
        self.running.set()
        self.data_state = None
        self.data_state_pt = None

        self._tx_queue_to_log = queue.Queue()
        self.mutex = threading.Semaphore(value=1)

        self._ts = datetime.datetime.utcnow()
        self._mongo_client = MongoClient(host=database_address, port=database_port)
        self._base = self._mongo_client[database_name]
        self._collection = self._base['log']

        if len(self._collection.index_information()) < 1:
            self._logger.info('Creating indexes in {} database'.format(database_name))
            self._collection.create_index([('id', ASCENDING)])
            self._collection.create_index([('ts', ASCENDING)])

        self._reader = threading.Thread(target=self.rx_thread)
        self._reader.daemon = True

        self._reader.start()

    def process_sigusr2(self):
        self._mode_auto = not self._mode_auto
        self._logger.info('Auto mode: {}'.format(self._mode_auto))

    def process_sighup(self):
        self._mode_auto = False
        f_json = open(self._program_path)
        self._mode_parser.parse_json(f_json)
        f_json.close()

    def add_to_send(self, proto_to_send):
        """Добавляет Protocol Buffer на отправку в устройство
        :param proto_to_send: Protocol Buffer (Protocol()) - Сообщение для отправки
        :return: None
        """
        # TODO А не нужен ли тут Mutex???
        for controller in self.vgfthermos:
            lp = VGFThermo_pb2.Payload()
            valid = False
            for htd in proto_to_send.HtD:
                if htd.ZoneNumber in controller.valid_zones:
                    # Тут есть валидная зона для данного контроллера
                    # self._logger.debug('Найдены данные для отправки в контроллер {}'.format(controller.DeviceId))
                    # self._logger.debug(htd)
                    l_htd = lp.HtD.add()
                    l_htd.MergeFrom(htd)
                    valid = True

            if valid:
                # self._logger.debug('Полные данные для отправки в контроллер {}'.format(controller.DeviceId))
                # self._logger.debug(lp)
                # Чистим очередь на отправку
                while not controller.tx_queue.empty():
                    controller.tx_queue.get()
                # Добавляем элемент на отправку
                controller.tx_queue.put(lp)

        # TODO Тут нужно отправить на запись лога в базу
        while not self._tx_queue_to_log.empty():
            self._tx_queue_to_log.get()

    def rx_thread(self):
        while self.running.is_set():
            msg = self.bus.recv(self.timeout)
            if msg is not None:
                if msg.arbitration_id == 0x80:
                    self.process_sync()
                else:
                    for thermo_controller in self.vgfthermos:
                        thermo_controller.process_in_message(msg)
                    self.controller_pt.process_in_message(msg)
                    self.display.process_in_message(msg)

    def process_sync(self):
        self._sync_number += 1

        self._ts = datetime.datetime.utcnow()
        lp = VGFThermo_pb2.Payload()
        lp_pt = self.controller_pt.get_data_for_logging()
        self.data_state_pt = lp_pt

        lp_disp = self.display.get_data_for_logging()

        n = 0
        cold_junction = self.controller_pt.cold_junction_temperature

        if self._mode_auto:
            thermo_payload = self._mode_parser.get_data()

            if thermo_payload is None:
                self._mode_auto = False
                self._logger.info('End of program')
            else:
                if not isinstance(thermo_payload, int):
                    if len(thermo_payload.HtD):
                        self.add_to_send(thermo_payload)

        for c in self.vgfthermos:
            c.cold_junction_temperature = cold_junction
            ll = c.get_data_for_logging()
            if ll is not None:
                lp.MergeFrom(ll)
                n += 1

        if n == 3:
            lp.TCold = cold_junction
            self.data_state = lp
            self.process_mongo_add(self, lp, lp_pt, lp_disp)

    def get_data_state(self):
        if self.mutex.acquire(blocking=True, timeout=0.01):
            lp = self.data_state
            lp_pt = self.data_state_pt
            self.mutex.release()
            return self._sync_number, lp, lp_pt
        return None

    def get_data_settings(self):
        """
        Собирает настройки с контроллеров температуры
        :return:
        """
        if self.mutex.acquire(blocking=True, timeout=0.01):
            protocol_ret = VGFThermo_pb2.Payload()
            for ctrl in self.vgfthermos:
                device_id = ctrl.DeviceId
                for z in ctrl.settings.HtD:
                    htd = protocol_ret.HtD.add()
                    htd.MergeFrom(z)
                    htd.ZoneNumber += (device_id - 1)*8

            self.mutex.release()
            return protocol_ret

        return None

    @staticmethod
    def process_mongo_add(self, proto_in, pt_in, disp_in):
        # TODO Вписать логгирование всех данных в Mongo
        zone_number = list(map(lambda x: x.ZoneNumber, proto_in.DtH))
        current_t = list(map(lambda x: x.CurrentT, proto_in.DtH))
        preset_t = list(map(lambda x: x.PresetT, proto_in.DtH))
        pwm_out = list(map(lambda x: x.PwmOut, proto_in.DtH))
        pid_error = list(map(lambda x: x.PidError, proto_in.DtH))
        sendiag = list(map(lambda x: x.Sendiag, proto_in.DtH))
        mode = list(map(lambda x: x.Mode, proto_in.DtH))

        if disp_in is not None:
            display_data = {
                'PowerU': disp_in.DtH.PowerVoltage,
                'PowerI': disp_in.DtH.PowerCurrent,
                'Pressure': disp_in.DtH.Pressure,
                'CPUTemp': disp_in.DtH.CPUTemp
            }
        else:
            display_data = {}

        if pt_in is not None:
            pt_data = {
                'Temperature': list(pt_in.DtH.TemperaturePt),
                'Sendiag': list(pt_in.DtH.SendiagPt),
                'TempDs': list(pt_in.DtH.TemperatureDs)
            }
        else:
            pt_data = {}

        insert_data = {
            'ts': self._ts,
            'id': 2,
            'ZoneNumber': zone_number,
            'CurrentT': current_t,
            'PresetT': preset_t,
            'PwmOut': pwm_out,
            'PidError': pid_error,
            'Sendiag': sendiag,
            'Mode': mode,

            'Platinum': pt_data,
            'Display': display_data
        }

        self._collection.insert(insert_data)
