# -*- coding: utf-8 -*-
__author__ = 'Vladimir Meshkov <glavmonter@gmail.com>'

import threading
import select
from VGF.protocol_tcp_pb2 import ClientServer, ClientStateReply, GET_CLIENT_STATE, SET_CLIENT_SETTINGS, GET_CLIENT_SETTINGS
import VGF.VGFThermo_pb2 as VGFThermo_pb2


class ClientThread(threading.Thread):
    def __init__(self, ip, port, socket, type, vgfmanager):
        threading.Thread.__init__(self)
        self._ip = ip
        self._port = port
        self._socket = socket
        self.type = type
        self.vgfmanager = vgfmanager
        self.running = threading.Event()
        self.running.set()

    def run(self):
        import logging
        logging.basicConfig(level=logging.DEBUG)
        self.logger = logging.getLogger('thread-{}'.format(self._ip))
        self.logger.setLevel(logging.DEBUG)
        self._socket.setblocking(0)

        try:
            self.logger.debug('Connected {} at {}'.format(self._ip, self._port))
            while self.running.is_set():
                ready = select.select([self._socket], [], [], 0.5)

                if ready[0]:
                    data = self._socket.recv(2048)
                    if len(data) == 0:
                        self.logger.debug('Remote host closed connection')
                        self.running.clear()
                        self._socket.close()
                        return

                    self.process_input_data(data)

        except:
            self.logger.exception('Problem handling request')
            self.running.clear()
            self._socket.close()

    def process_input_data(self, data):
        protocol_in = ClientServer()
        protocol_in.ParseFromString(data)

        if protocol_in.type != self.type:
            self.logger.debug('Клиент прислал какую-то бяку в типе {}'.format(protocol_in.type))
            return

        if protocol_in.HasField('SetData'):
            if protocol_in.SetData == SET_CLIENT_SETTINGS:
                # TODO Тут нужно что-то сделать и передать данные в менеджер

                if protocol_in.HasField('Thermo'):
                    payload_thermo = VGFThermo_pb2.Payload()
                    payload_thermo.ParseFromString(protocol_in.Thermo)

                self.vgfmanager.add_to_send(payload_thermo)

        if protocol_in.HasField('GetData'):
            if protocol_in.GetData == GET_CLIENT_STATE:
                server_reply = ClientStateReply()
                server_reply.GetData = GET_CLIENT_STATE

                sync_number, lp, lp_pt = self.vgfmanager.get_data_state()
                if lp is not None:
                    server_reply.Thermos = lp.SerializeToString()

                if lp_pt is not None:
                    # print(lp_pt)
                    server_reply.Platinum = lp_pt.SerializeToString()

                server_reply.SyncNumber = sync_number
                self._socket.send(server_reply.SerializeToString())

            elif protocol_in.GetData == GET_CLIENT_SETTINGS:
                self.logger.info('Начальные настройки')
                server_reply = ClientStateReply()
                server_reply.GetData = GET_CLIENT_SETTINGS
                lp = self.vgfmanager.get_data_settings()
                # self.logger.debug(lp)

                if lp is not None:
                    lp.DeviceId = 1
                    server_reply.Thermos = lp.SerializeToString()

                    self.logger.info('Успешно получил настройки')
                    self._socket.send(server_reply.SerializeToString())
