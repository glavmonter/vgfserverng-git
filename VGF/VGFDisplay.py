# -*- coding: utf-8 -*-
from VGF.VGFBase import VGFBase
import VGF.VGFDisplay_pb2 as VGFDisplay_pb2
import queue
__author__ = 'Vladimir Meshkov <glavmonter@gmail.com>'


class DisplayClass(VGFBase):
    def __init__(self, DeviceId, bus):
        super().__init__(DeviceId, bus)

        import logging
        self._logger = logging.getLogger('Thermo_{}'.format(DeviceId))
        self._logger.setLevel(logging.INFO)

        self._payload = None

    def deserialize_input_buffer(self, in_bytes):
        """Десериализация входного потока данных
        :param in_bytes: bytes, буфер из входных данных
        :return: Protocol Buffer - Класс (Protocol) десериализованных данных
        """
        self._payload = VGFDisplay_pb2.Payload()
        self._payload.ParseFromString(in_bytes)
        return self._payload

    def get_data_to_send(self):
        return None

    def process_emergency_msg(self, msg):
        pass

    def get_data_for_logging(self):
        if self.received_protos.qsize() > 0:
            lp = self.received_protos.get()
            return lp
        return None
