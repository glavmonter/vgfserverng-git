# -*- coding: utf-8 -*-
__author__ = 'Vladimir Meshkov <glavmonter@gmail.com>'

import queue
from ctypes import CDLL, POINTER, c_int, byref, sizeof
from errno import ENOBUFS

from can.message import Message
from can.interfaces.interface import CAN_FRAME

libc = CDLL("libc.so.6")

get_errno_loc = libc.__errno_location
get_errno_loc.restype = POINTER(c_int)


def l_errno():
    return get_errno_loc()[0]


def _build_can_frame(message):
    arbitration_id = message.arbitration_id
    if message.id_type:
        arbitration_id |= 0x80000000
    if message.is_remote_frame:
        arbitration_id |= 0x40000000
    if message.is_error_frame:
        arbitration_id |= 0x20000000

    # TODO need to understand the extended frame format
    frame = CAN_FRAME()
    frame.can_id = arbitration_id
    frame.can_dlc = len(message.data)

    for i in range(len(message.data)):
        frame.data[i] = message.data[i]
    return frame


def send_packet(socket, message):
    frame = _build_can_frame(message)
    err = 0
    while True:
        bytes_sent = libc.write(socket, byref(frame), sizeof(frame))
        err = libc.__errno_location()[0]
        if bytes_sent != -1:
            break

        if err != ENOBUFS:
            break

    return bytes_sent, err


class VGFBase(object):
    """Базовый класс для получения и передачи данных по шине CAN"""
    CANMSG_EMERGENCY = 0x07  # нижние 4 бит - 0, максимальный приоритет, 1 DLC
    CANMSG_SYNC = 0x08  # нижние 4 бит - 0, приоритет пониже emcy, 0 DLC
    CANMSG_ACK = 0x09  # нижние 4 бит - id куда, 0 DLC
    CANMSG_NACK = 0x0A  # нижние 4 бит - id куда, 0 DLC
    CANMSG_DTH = 0x0B  # нижние 4 бит - id куда, 1-4 DLC
    CANMSG_HTD = 0x0C  # нижние 4 бит - id откуда, 1-4 DLC
    CANMSG_HEARTBEAT = 0x0D  # нижние 4 бит - id откуда, 0 DLC
    CANMSG_DATA = 0x0E  # нижние 4 бит - id куда, ? DLC

    def __init__(self, DeviceId, bus):
        self._bus = bus
        self.DeviceId = DeviceId

        import logging
        self._logger = logging.getLogger('Base_{}'.format(DeviceId))
        self._logger.setLevel(logging.DEBUG)

        self.bytes_to_receive = 0
        self.num_msg = 0
        self.received_msg = 0
        self.inBuffer = bytearray()
        self.tx_queue = queue.Queue()
        self.received_protos = queue.Queue()
        self.cold_junction_temperature = 24.0

    @staticmethod
    def GENID(self, cobid, canmsg):
        return (canmsg << 4) + cobid

    @staticmethod
    def GETMSG(self, cobid):
        return (cobid >> 4) & 0x0F

    @staticmethod
    def GETID(self, cobid):
        return cobid & 0x0F

    def push_tx_data(self, data):
        self.tx_queue.put_nowait(data)

    def __call__(self, msg):
        self.process_in_message(msg)

    def process_in_message(self, msg):
        """Функция обработки входящего пакета CAN.
        После получения всего потока данных функция отправляет данные на устройсвто, если они есть
        :param msg: Пакет CAN
        :return: None
        """
        if self.GETID(self, msg.arbitration_id) != self.DeviceId:
            return

        if self.GETMSG(self, msg.arbitration_id) == self.CANMSG_EMERGENCY:
            self.process_emergency_msg(msg)

            if (msg.data[0] not in [10, 11]) and (msg.data[1] != 255):
                self._logger.error('Device {} Emergency!! msg: {}'.format(self.DeviceId, msg))

            if msg.data[0] in range(0, 8):
                """
                Ошибка в зонах.
                """
                if msg.data[1] == 1:  # Ошибка приема сообщения от АЦП
                    self._logger.error('Device {}, zone {}, ADCIn error'
                                       .format(self.DeviceId, msg.data[0]))

                if msg.data[1] == 2:  # Ошибка приема настроек
                    self._logger.error('Device {}, zone {}, Settings in error'
                                       .format(self.DeviceId, msg.data[0]))

                if msg.data[1] == 3:  # Ошибка таймаута зоны
                    self._logger.error('Device {}, zone {}, Timeout: {}'
                                       .format(self.DeviceId, msg.data[0],
                                               (msg.data[2] << 8) + msg.data[3]))

            if msg.data[0] == 8:
                self._logger.error('Stack overflow: {}'.format(msg))

            if msg.data[0] == 9:  # CANTask
                if msg.data[1] == 1:  # Ошибка приема статуса зоны
                    self._logger.error('Device {}, CanTask Прием статуса зоны {}'
                                       .format(self.DeviceId, msg.data[2]))
                if msg.data[1] == 2:
                    self._logger.error('Device {}, CanTask Отправка настроек в зону {}'
                                       .format(self.DeviceId, msg.data[2]))

            if msg.data[0] in [10, 11]:  # ADC_1
                if msg.data[1] == 1:
                    self._logger.error('Device {}, ADC {}, ISR error'
                                       .format(self.DeviceId, msg.data[0] - 9))
                if msg.data[1] == 2:
                    self._logger.error('Device {}, ADC {}, Ошибка очереди данных: {}'
                                       .format(self.DeviceId, msg.data[0] - 9, msg.data[2]))
                if msg.data[1] == 3:
                    self._logger.error('Device {}, ADC {}, CRC error. Calc: {}, Rcv: {}'
                                       .format(self.DeviceId, msg.data[0] - 9, hex(msg.data[2]), hex(msg.data[3])))
                if msg.data[1] == 4:
                    self._logger.error('Device {}, ADC {}, TCold error'
                                       .format(self.DeviceId, msg.data[0] - 9))
                if msg.data[1] == 5:
                    self._logger.error('Device {}, ADC {}, Timeout: {}'
                                       .format(self.DeviceId, msg.data[0] - 9,
                                               (msg.data[2] << 8) + msg.data[3]))

                # if msg.data[1] == 255:
                #     self._logger.debug('Device {}, ADC {} running normally'
                #                        .format(self.DeviceId, msg.data[0] - 9))

            if (msg.data[0] not in [10, 11]) and (msg.data[1] != 255):
                self._logger.error('')

        if self.GETMSG(self, msg.arbitration_id) == self.CANMSG_DTH:
            self.bytes_to_receive = msg.data[0]
            self.bytes_to_receive += msg.data[1]
            self.num_msg = self.bytes_to_receive // 7
            if self.bytes_to_receive % 7:
                self.num_msg += 1
            self.received_msg = self.num_msg

            self.inBuffer = bytearray(self.bytes_to_receive)

        if self.GETMSG(self, msg.arbitration_id) == self.CANMSG_DATA:
            index = msg.data[0]
            if index >= self.num_msg:
                self.inBuffer.clear()
                return

            in_len = msg.dlc - 1
            self.inBuffer[index * 7:index * 7 + in_len] = msg.data[1:]
            self.received_msg -= 1

            if self.received_msg == 0:

                msg = Message()
                msg.arbitration_id = self.GENID(self, self.DeviceId, self.CANMSG_ACK)
                msg.dlc = 0
                msg.id_type = False
                bytes_sent, err = send_packet(self._bus.socket, msg)
                if bytes_sent == -1:
                    self._logger.warning('Id: {} Ошибка отправки ACK'.format(self.DeviceId))

                # Тут мы получили оформленный Protocol от устройства
                pp = self.deserialize_input_buffer(bytes(self.inBuffer))
                self.received_protos.put(pp)
                self.send_to_device(self.get_data_to_send())

    def send_to_device(self, data_to_send):
        """Отправка данных на устройство.
        :param data_to_send: bytes - Сериализованные данные Protocol Buffer для передачи
        :return: None
        """
        if data_to_send is None:
            return

        num_byte_to_send = len(data_to_send)
        num_messages = num_byte_to_send // 7
        msg = Message()

        msg.arbitration_id = self.GENID(self, self.DeviceId, self.CANMSG_HTD)
        msg.dlc = 6
        msg.data = [num_byte_to_send & 0xFF, (num_byte_to_send & 0xFF00) >> 8, 0, 0, 0, 0]
        msg.id_type = False

        bytes_sent, err = send_packet(self._bus.socket, msg)
        if bytes_sent == -1:
            self._logger.warning('Id: {}. Ошибка при отправке первого пакета'.format(self.DeviceId))

        for m in range(num_messages):
            mmm = Message()
            mmm.arbitration_id = self.GENID(self, self.DeviceId, self.CANMSG_DATA)
            mmm.dlc = 8
            mmm.id_type = False

            mmm.data = bytearray(1)
            mmm.data[0] = m
            mmm.data += bytearray(data_to_send[m * 7: (m + 1) * 7])
            bytes_sent, err = send_packet(self._bus.socket, mmm)
            if bytes_sent == -1:
                self._logger.warning('Id: {}. Ошибка при отправке {} пакета, err: {}'.format(self.DeviceId, m, err))

        if num_byte_to_send % 7:
            mmm = Message()
            mmm.arbitration_id = self.GENID(self, self.DeviceId, self.CANMSG_DATA)
            mmm.dlc = 1 + num_byte_to_send % 7
            mmm.id_type = False

            mmm.data = bytearray(1)
            mmm.data[0] = num_byte_to_send // 7
            mmm.data += bytearray(data_to_send[num_byte_to_send - num_byte_to_send % 7: num_byte_to_send])
            bytes_sent, err = send_packet(self._bus.socket, mmm)
            if bytes_sent == -1:
                self._logger.warning('Id: {}. Ошибка при отправке последнего пакета'.format(self.DeviceId))

    def get_data_to_send(self):
        raise NotImplementedError(
            '{} has not implemented get_data_to_send'.format(
                self.__class__.__name__
            )
        )

    def deserialize_input_buffer(self, in_bytes):
        """Десериализация входного потока данных
        :param in_bytes: bytes, буфер из входных данных
        :return: Protocol Buffer - Класс десериализованных данных
        """
        raise NotImplementedError(
            "{} has not implemented deserialize_input_buffer".format(
                self.__class__.__name__
            )
        )

    def process_emergency_msg(self, msg):
        """
        Отображение сообщения об ошибке в логи
        :param msg: canmsg сообщение
        :return: None
        """
        raise NotImplementedError(
            "{} has not implement process_emergency_msg".format(
                self.__class__.__name__
            )
        )
