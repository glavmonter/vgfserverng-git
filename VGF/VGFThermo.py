# -*- coding: utf-8 -*-
__author__ = 'Vladimir Meshkov <glavmonter@gmail.com>'

from VGF.VGFBase import VGFBase
import VGF.VGFThermo_pb2 as VGFThermo_pb2
import queue


class ThermoClass(VGFBase):
    """Класс, отвечающий за работу с контроллером VGFCtrl"""
    def __init__(self, DeviceId, bus):
        VGFBase.__init__(self, DeviceId, bus)  # TODO Заменить на super()
        self._payload = None
        self.valid_zones = list(range((self.DeviceId - 1)*8, self.DeviceId*8))

        import logging
        self._logger = logging.getLogger('Thermo_{}'.format(DeviceId))
        self._logger.setLevel(logging.INFO)

        self.settings = VGFThermo_pb2.Payload()
        self.settings.DeviceId = self.DeviceId

        for n in range(8):
            htd = self.settings.HtD.add()
            htd.ZoneNumber = n
            htd.PresetT = 10.0
            htd.PwmMaximum = 0
            htd.PwmOut = 0
            htd.Kp = 0.0
            htd.Ki = 0.0
            htd.Kd = 0.0
            htd.Mode = VGFThermo_pb2.MODE_STOPPED

    def deserialize_input_buffer(self, in_bytes):
        """Десериализация входного потока данных
        :param in_bytes: bytes, буфер из входных данных
        :return: Protocol Buffer - Класс (Protocol) десериализованных данных
        """
        self._payload = VGFThermo_pb2.Payload()
        self._payload.ParseFromString(in_bytes)
        for i in range(8):
            self._payload.DtH[i].ZoneNumber += (self.DeviceId-1)*8

        # print(self._payload)
        return self._payload

    def get_data_to_send(self):
        """Тут находим свои данные и отправляем
        :return: None
        """
        try:
            tx = self.tx_queue.get_nowait()
            self._logger.debug('Device {}. tx: {}'.format(self.DeviceId, tx))
            # Мержим настройки
            for new_htd in tx.HtD:
                new_htd.ZoneNumber -= (self.DeviceId - 1)*8
                self.settings.HtD[new_htd.ZoneNumber].MergeFrom(new_htd)

            tx.TCold = self.cold_junction_temperature
            tx.DeviceId = self.DeviceId
            if self.DeviceId == 1:
                self._logger.debug('----Tx [{}] Full-----'.format(self.DeviceId))
                self._logger.debug(tx)
            elif self.DeviceId == 2:
                self._logger.debug('----Tx [{}] Full-----'.format(self.DeviceId))
                self._logger.debug(tx)
            elif self.DeviceId == 3:
                self._logger.debug('----Tx [{}] Full-----'.format(self.DeviceId))
                self._logger.debug(tx)

            return tx.SerializeToString()

        except queue.Empty:
            tx = VGFThermo_pb2.Payload()
            tx.DeviceId = self.DeviceId
            tx.TCold = self.cold_junction_temperature

            # self._logger.debug('----Tx reduced {}-----'.format(self.DeviceId))
            # self._logger.debug(tx)
            return tx.SerializeToString()

    def get_data_for_logging(self):
        if self.received_protos.qsize() > 0:
            lp = self.received_protos.get()
            return lp
        return None

    def process_emergency_msg(self, msg):
        pass
