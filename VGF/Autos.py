# -*- coding: utf-8 -*-
import json
import VGF.VGFThermo_pb2 as VGFThermo_pb2
from datetime import datetime, timedelta
__author__ = 'Vladimir Meshkov <vld.meshkov@gmail.com>'

empty_strings = 2*'\n'


def from_seconds(seconds):
    mm, ss = divmod(seconds, 60)
    hh, mm = divmod(mm, 60)
    dd, hh = divmod(hh, 24)
    return dd, hh, mm, ss


class ModeParser(object):
    def __init__(self):
        import logging
        self._logger = logging.getLogger('Parser')
        self._logger.setLevel(logging.INFO)

        file_handler = logging.FileHandler(filename='/tmp/test.log', mode='a')
        file_handler.setLevel(logging.INFO)
        formatter = logging.Formatter(fmt='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                                      datefmt='%Y-%m-%d, %H:%M:%S')
        file_handler.setFormatter(formatter)
        self._logger.addHandler(file_handler)

        self._json = None
        self._list_program = []
        self._tick = 0
        self._step = 0
        self._current_step = 0

        self.logger_mode = logging.getLogger('Mode')
        self.logger_mode.setLevel(logging.INFO)
        file_handler = logging.FileHandler(filename='/tmp/test.log', mode='a')
        file_handler.setLevel(logging.INFO)
        formatter = logging.Formatter(fmt='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                                      datefmt='%Y-%m-%d, %H:%M:%S')
        file_handler.setFormatter(formatter)
        self.logger_mode.addHandler(file_handler)

    def parse_json(self, data):
        self._list_program.clear()
        self._json = json.load(data)
        self._step = self._json['step'] - 1
        self._current_step = self._step

        seconds = 0
        for program in self._json['program']:
            if program['type'] == 'preheat':
                self._list_program.append(ModePreheate(program, self._step, self.logger_mode))
            elif program['type'] == 'none':
                self._list_program.append(ModeNone(program, self._step, self.logger_mode))
            elif program['type'] == 'gradcooling':
                self._list_program.append(ModeGradientCooling(program, self._step, self.logger_mode))
            elif program['type'] == 'step':
                self._list_program.append(ModeStep(program, self._step, self.logger_mode))

            if len(self._list_program) > 0:
                seconds += self._list_program[len(self._list_program) - 1].seconds

        self._logger.info('Initialize Auto mode')
        self._logger.info('Found {} steps program'.format(len(self._list_program)))
        dd, hh, mm, ss = from_seconds(seconds)
        if dd > 0:
            self._logger.info('Full time of program: {} days, {} hours, {} minutes'.format(dd, hh, mm))
        else:
            self._logger.info('Full time of program: {} hours, {} minutes, {} seconds'.format(hh, mm, ss))

    def get_data(self):
        if len(self._list_program) == 0:
            return None

        if self._current_step == 0:
            self._current_step = self._step

            mode = self._list_program[0]
            if mode.is_valid():
                return mode.get_proto()
            else:
                self._list_program.remove(mode)

                if len(self._list_program) > 0:
                    mode = self._list_program[0]
                    return mode.get_proto()
                else:
                    return None
        else:
            self._current_step -= 1
        return int(0)


class Mode(object):
    def __init__(self, data, step, logger):
        self.first_start = True
        self.valid = 10
        self.data = data
        self.step = step + 1

        self.ThermoPayload = VGFThermo_pb2.Payload()
        self.ThermoPayload.DeviceId = 1

        self.logger = logger

    def is_valid(self):
        """Проверка, что режим еще валидный
        :return: True - Можно брать данные, False - пора переходить к новому заданию
        """
        raise NotImplementedError

    def get_proto(self):
        """Получить новую порцию Protobuf
        :return: Protobuf для задания зон
        """
        raise NotImplementedError

    def get_htd(self):
        valid_htd = False
        htd = VGFThermo_pb2.HostToDevice()
        proto_keys = list(map(lambda x: x.name, htd.DESCRIPTOR.fields))

        for js_key in self.data['Proto'].keys():
            value = self.data['Proto'][js_key]
            if js_key == 'Mode':
                if self.data['Proto']['Mode'] == 'Stopped':
                    value = VGFThermo_pb2.MODE_STOPPED
                elif self.data['Proto']['Mode'] == 'Manual':
                    value = VGFThermo_pb2.MODE_MANUAL
                elif self.data['Proto']['Mode'] == 'Relay':
                    value = VGFThermo_pb2.MODE_RELAY
                elif self.data['Proto']['Mode'] == 'PID':
                    value = VGFThermo_pb2.MODE_PID
                else:
                    value = VGFThermo_pb2.MODE_STOPPED

            if js_key in proto_keys:
                setattr(htd, js_key, value)
                valid_htd = True

        if valid_htd:
            htd.ZoneNumber = 0
            return htd
        else:
            return None


class ModePreheate(Mode):
    def __init__(self, data, step, logger):
        super().__init__(data, step, logger)
        self._start_t = self.data['StartT']
        self._stop_t = self.data['StopT']
        self._speed = self.data['Speed'] / 3600.0
        self._name = self.data['name']
        self._current_time = 0

        self.valid = int((self._stop_t - self._start_t)/self._speed/self.step) + 2
        self.seconds = self.valid*self.step

        first_htd = self.get_htd()
        self._first_proto = VGFThermo_pb2.Payload()
        self._first_proto.DeviceId = 1
        for z in range(24):
            htd = self._first_proto.HtD.add()
            htd.MergeFrom(first_htd)
            htd.ZoneNumber = z

            htd = self.ThermoPayload.HtD.add()
            htd.ZoneNumber = z

    def get_proto(self):
        T = self._start_t + self._speed*self._current_time
        if self._speed > 0.0:
            if T > self._stop_t:
                T = self._stop_t
        else:
            if T < self._stop_t:
                T = self._stop_t

        self._current_time += self.step

        if self.first_start:
            dd, hh, mm, ss = from_seconds(self.seconds)
            self.logger.info('Step name: {}'.format(self._name))
            if self._speed > 0.0:
                self.logger.info('Starting preheating from {} C to {} C, speed {} C/hour'.format(
                    self._start_t, self._stop_t, self.data['Speed']))
                self.logger.info('Preheating time: {} days, {} hours and {} minutes'.format(dd, hh, mm))
                end_time = timedelta(seconds=self.seconds) + datetime.now()
                self.logger.info('End of preheating at {}{}'.format(
                    datetime.strftime(end_time, '%Y-%m-%d, %H:%M:%S'), empty_strings))
            else:
                self.logger.info('Starting cooldowning from {} C to {} C, speed {} C/hour'.format(
                    self._start_t, self._stop_t, -self.data['Speed']))
                self.logger.info('Cooldowning time: {} days, {} hours and {} minutes'.format(dd, hh, mm))
                end_time = timedelta(seconds=self.seconds) + datetime.now()
                self.logger.info('End of colldowning at {}'.format(
                    datetime.strftime(end_time, '%Y-%m-%d, %H:%M:%S'), empty_strings))

            self.first_start = False
            self.valid -= 1
            for z in self._first_proto.HtD:
                z.PresetT = T
            return self._first_proto

        elif self.is_valid():
            self.valid -= 1
            for z in self.ThermoPayload.HtD:
                z.PresetT = T
            return self.ThermoPayload

    def is_valid(self):
        return self.valid > 0


class ModeNone(Mode):
    def __init__(self, data, step, logger):
        super().__init__(data, step, logger)
        self._name = self.data['name']
        self.seconds = self.data['time']
        self.valid = self.seconds/self.step + 2

    def get_proto(self):
        if self.first_start:
            self.first_start = False
            self.valid -= 1
            dd, hh, mm, ss = from_seconds(self.seconds)
            end_time = datetime.now() + timedelta(seconds=self.seconds)
            self.logger.info('Step name: {}'.format(self._name))
            self.logger.info('Starting empty mode for {} days, {} hours and {} minutes'.format(dd, hh, mm))
            self.logger.info('End of empty program at {}{}'.format(
                datetime.strftime(end_time, '%Y-%m-%d, %H:%M:%S'), empty_strings))
            return self.ThermoPayload

        if self.is_valid():
            self.valid -= 1
            return self.ThermoPayload
        else:
            return None

    def is_valid(self):
        return self.valid > 0


class ModeStep(Mode):
    def __init__(self, data, step, logger):
        super().__init__(data, step, logger)
        self.valid = 1
        self._name = self.data['name']
        self._first_htd = self.get_htd()
        self.seconds = 0

        self.ThermoPayload.DeviceId = 1
        for z in range(24):
            htd = self.ThermoPayload.HtD.add()
            htd.MergeFrom(self._first_htd)
            htd.ZoneNumber = z

    def get_proto(self):
        if not self.is_valid():
            return None

        self.logger.info('Step name: {}'.format(self._name))
        self.logger.info('Program Step {}'.format(empty_strings))
        self.first_start = False
        self.valid = 0
        return self.ThermoPayload

    def is_valid(self):
        return self.valid > 0


class ModeGradientCooling(Mode):
    def __init__(self, data, step, logger):
        super().__init__(data, step, logger)
        self._name = self.data['name']
        self._first_htd = self.get_htd()

        self._k1 = self.data['K1']
        self._k2 = self.data['K2']
        self._k3 = self.data['K3']
        self._high_t = self.data['HighT']
        self._melt_t = self.data['MeltT']
        self._low_t = self.data['LowT']
        self._pos_start = self.data['PositionStart']
        self._pos_stop = self.data['PositionStop']
        self._pos_current = self._pos_start
        self._speed = self.data['Speed']/3600.0

        self._themperatures = [20.0 for i in range(24)]
        self._current_time = 0

        self.valid = int((self._pos_stop - self._pos_start)/self._speed/self.step) + 1
        self.seconds = self.valid*self.step
        self.logger.debug('valid: {}'.format(self.valid))
        self.logger.debug('seconds: {}'.format(self.seconds))

        first_htd = self.get_htd()
        self._first_proto = VGFThermo_pb2.Payload()
        self._first_proto.DeviceId = 1
        for z in range(24):
            htd = self._first_proto.HtD.add()
            htd.MergeFrom(first_htd)
            htd.ZoneNumber = z

            htd = self.ThermoPayload.HtD.add()
            htd.ZoneNumber = z

        self.logger.debug('first_proto: {}'.format(self._first_proto))
        self.logger.debug('ThermoPayload: {}'.format(self.ThermoPayload))

    def get_proto(self):
        zone_size = 10.0
        self._pos_current = self._pos_start + self._speed*self._current_time
        self.logger.debug('Position: {}'.format(self._pos_current))

        for zone in range(len(self._themperatures)):
            t = self._k1*10.0/zone_size*((zone + 1)*zone_size - self._pos_current) + self._melt_t;
            if t > self._high_t:
                t = self._high_t

            if t < self._low_t:
                delta = self._k3*10.0/zone_size*((zone + 2)*zone_size - self._pos_current);
                t = self._low_t + delta
            self._themperatures[zone] = t

        self._current_time += self.step

        if self.first_start:
            self.logger.info('Step name: {}'.format(self._name))
            dd, hh, mm, ss = from_seconds(self.seconds)
            self.logger.info('Starting gradient cooldowning from {} mm to {} mm, speed {:.1f} mm/hour'.format(
                self._pos_start, self._pos_stop, self.data['Speed']))
            self.logger.info('Gradient cooldowning time: {} days, {} hours, {} minutes'.format(dd, hh, mm))
            end_time = timedelta(seconds=self.seconds) + datetime.now()
            self.logger.info('End of Gradient cooldowning at {}{}'.format(
                datetime.strftime(end_time, '%Y-%m-%d, %H:%M:%S'), empty_strings))
            self.first_start = False
            self.valid -= 1

            for z in range(len(self._themperatures)):
                self._first_proto.HtD[z].PresetT = self._themperatures[z]

            self.logger.debug('Gradient first: {}'.format(self._first_proto))
            return self._first_proto

        elif self.is_valid():
            self.valid -= 1
            for z in range(len(self._themperatures)):
                self.ThermoPayload.HtD[z].PresetT = self._themperatures[z]

            self.logger.debug(self.ThermoPayload)
            return self.ThermoPayload
        else:
            return None

    def is_valid(self):
        return self.valid > 0


def test_program():
    parser = ModeParser()
    ff = open('program.json')
    parser.parse_json(ff)
    ff.close()

    for i in range(20000):
        ret = parser.get_data()
        # if ret is not None:
        #     if not isinstance(ret, int):
        #         print('i: {} = {}'.format(i, ret))

