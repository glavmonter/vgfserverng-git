# -*- coding: utf-8 -*-
__author__ = 'Vladimir Meshkov <glavmonter@gmail.com>'

from VGF.VGFBase import VGFBase
import VGF.VGFPlatinum_pb2 as VGFPlatinum_pb2

from math import exp

poly = [-0.176004136860E-01,
        0.389212049750E-01,
        0.185587700320E-04,
        -0.994575928740E-07,
        0.318409457190E-09,
        -0.560728448890E-12,
        0.560750590590E-15,
        -0.320207200030E-18,
        0.971511471520E-22,
        -0.121047212750E-25]

a0 = 0.118597600000E+00
a1 = -0.118343200000E-03
a2 = 0.126968600000E+03


def t_to_emf(tin):
    emf = 0.0
    for i in range(len(poly)):
        emf += poly[i]*(tin**i)
    emf += a0*exp(a1*((tin - a2)**2))
    return emf


class PlatinumClass(VGFBase):
    def __init__(self, DeviceId, bus):
        VGFBase.__init__(self, DeviceId, bus)
        self._payload = None
        self.cold_junction = 0.0

    def deserialize_input_buffer(self, in_bytes):
        self._payload = VGFPlatinum_pb2.Payload()
        self._payload.ParseFromString(in_bytes)
        # print(self._payload)

        not_null_ds = list(filter(lambda x: x > 0.3, self._payload.DtH.TemperatureDs[4: -1]))
        if len(not_null_ds) > 0:
            cold_junction = sum(not_null_ds)/len(not_null_ds)
            self.cold_junction_temperature = t_to_emf(cold_junction)*1000.0
            # self._logger.info('Холодный конец: {} °C - {} мкВ'.format(cold_junction, self.cold_junction_temperature))

        return self._payload

    def get_data_for_logging(self):
        if self.received_protos.qsize() > 0:
            lp = self.received_protos.get()
            return lp
        return None

    def get_data_to_send(self):
        return None

    def get_cold_junction(self):
        return self.cold_junction

    def process_emergency_msg(self, msg):
        pass
