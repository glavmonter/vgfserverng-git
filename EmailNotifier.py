# -*- coding: utf-8 -*-

smtpserver = 'smtp.gmail.com'

sender = 'sender@gmail.com'
destination = ['dummy@gmail.com']

USERNAME = 'loginname@gmail.com'
PASSWORD = 'mypassword'

text_subtype = 'plain'

content = """\
Test message to mani receivers.
Привет по русски
"""

subject = 'Sent from Python'

from smtplib import SMTP as SMTP
from email.mime.text import MIMEText

try:
    msg = MIMEText(content, text_subtype)
    msg['Subject'] = 'Отправлено из Python'
    msg['From'] = 'from@gmail.com'
    msg['To'] = ', '.join(destination)

    conn = SMTP("smtp.gmail.com", 587, timeout=1)
    conn.set_debuglevel(True)
    conn.ehlo()
    conn.starttls()
    conn.ehlo()
    conn.login(USERNAME, PASSWORD)

    try:
        conn.sendmail(from_addr=sender, to_addrs=destination, msg=str(msg))
    finally:
        conn.close()

except Exception as exc:
    print('mail failed: {}'.format(str(exc)))

