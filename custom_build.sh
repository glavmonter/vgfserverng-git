#!/bin/sh
rm VGF/*_pb2.py

PROTO_COMPILER=/opt/protobuf-py3/bin/protoc

$PROTO_COMPILER -I ../protos/ --python_out=. ../protos/VGFThermo.proto
$PROTO_COMPILER -I ../protos/ --python_out=. ../protos/VGFPlatinum.proto
$PROTO_COMPILER -I ../protos/ --python_out=. ../protos/VGFDisplay.proto
$PROTO_COMPILER -I ../protos/ --python_out=. ../protos/protocol_tcp.proto

mv VGFThermo_pb2.py VGF/
mv VGFPlatinum_pb2.py VGF/
mv VGFDisplay_pb2.py VGF/
mv protocol_tcp_pb2.py VGF/

