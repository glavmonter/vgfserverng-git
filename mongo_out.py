#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from pymongo import MongoClient, DESCENDING
from configparser import ConfigParser

parser = ConfigParser()
parser.read('settings.ini')
log_database = parser.get('server', 'log_database')

mongo_client = MongoClient(host='127.0.0.1')
mongo_base = mongo_client[log_database]
mongo_collection = mongo_base['log']

posts = mongo_collection.find().sort('ts', DESCENDING).limit(1000)
for post in posts:
    print(' '.join(map(str, post['CurrentT'])))
